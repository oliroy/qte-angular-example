'use strict';

/**
 * @ngdoc directive
 * @name qteApp.directive:contactItem
 * @description
 * # contactItem
 */
angular.module('qteApp')
    .directive('qteContactItem', function () {
        return {
            templateUrl: 'templates/qte-contact-item.html',
            restrict: 'AE',
            scope: {
                contact: '=', // binds to the object from the parent scope
                contactsModel: '=', //need the model from the controller as ng-repeat does not update the controller model when its own model changes
                contactIndex: '='
            },
            compile: function () {
                return {
                    pre: function ($scope, element, attrs) {
                        $scope.validate = function (d, type) {
                            var pattern;
                            switch (type) {
                                case 'name':
                                case 'address':
                                    pattern = /^[a-zA-Z0-9\s]*$/;
                                    break;
                                case 'phone':
                                    pattern = /^[0-9\-]*$/;
                                    break;
                                default:
                                    pattern = /^[a-zA-Z0-9\s]*$/;
                            }
                            ;

                            if (pattern.test(d)) {
                                return true;
                            }
                            return false;
                        };
                        $scope.deleteContact = function () {
                            $scope.contactsModel.splice($scope.contactIndex, 1);
                        };

                    },
                    post: function ($scope, element, attrs) {
                    }
                };
            },
            link: function () {
            }
        };
    });
