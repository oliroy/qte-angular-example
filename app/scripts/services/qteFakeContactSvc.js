'use strict';

/**
 * @ngdoc service
 * @name questradeExampleApp.qteFakeContact
 * @description
 * # qteFakeContact
 * Service in the questradeExampleApp.
 */
angular.module('qteApp')
    .service('qteFakeContactSvc', function qteFakeContactSvc() {
        /*
         Revealing Module Pattern
         */
        var getContacts = function () {
            return [
                {
                    name: 'John Smith',
                    address: '22 Toronto',
                    phone: '528-231-5941',
                    properties: {
                        show: true
                    }
                },
                {
                    name: 'Dan McDonald',
                    address: '82 Toronto',
                    phone: '345-324-9123',
                    properties: {
                        show: true
                    }
                },
                {
                    name: 'Alex Myth',
                    address: '12 Toronto',
                    phone: '315-324-9123',
                    properties: {
                        show: true
                    }
                },
                {
                    name: 'Alex Joe',
                    address: '51 Oakville',
                    phone: '905-324-2213',
                    properties: {
                        show: true
                    }
                },
                {
                    name: 'Victoria Wam',
                    address: '51 Richmond',
                    phone: '905-222-2213',
                    properties: {
                        show: true
                    }
                },
                {
                    name: 'Stephanie Ann',
                    address: '22 Richmond',
                    phone: '905-222-8251',
                    properties: {
                        show: true
                    }
                }
            ];
        };
        /*
        Creates a contact object when passed a contactForm model
         */
        var createContact = function (contactForm) {
            return {
                name: contactForm.name.text,
                address: contactForm.address.text,
                phone: contactForm.phone.text,
                properties: {
                    show: true
                }
            };
        };
        return {
            getContacts: getContacts,
            createContact: createContact
        };
    });
