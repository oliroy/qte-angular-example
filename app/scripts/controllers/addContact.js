'use strict'

/**
 * @ngdoc function
 * @name qteApp.controller:AddContact
 * @description
 * # AddContact
 * Controller of the qteApp
 */
angular.module('qteApp')
    .controller('AddContactCtrl', function ($scope, $modalInstance) {

        $scope.model = {};
        $scope.model.newContact = {
            name: {
                text: "",
                pattern: /^[a-zA-Z0-9\s]*$/
            },
            address: {
                text: "",
                pattern: /^[a-zA-Z0-9\s]*$/
            },
            phone: {
                text: "",
                pattern: /^[0-9\-]*$/
            }
        };

        $scope.add = function () {
            $modalInstance.close($scope.model.newContact);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    });

