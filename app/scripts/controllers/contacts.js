'use strict';

/**
 * @ngdoc function
 * @name qteApp.controller:ContactsCtrl
 * @description
 * # ContactsCtrl
 * Controller of the qteApp
 */
angular.module('qteApp')
    .controller('ContactsCtrl', function ($scope, qteFakeContactSvc, $modal, $log) {

        /* Controller Model
         */
        $scope.model = {};
        $scope.model.search = {
            text: "",
            pattern: /^[a-zA-Z0-9\-\s]*$/
        };
        $scope.model.newContact = {
            name: {
                text: "",
                pattern: /^[a-zA-Z0-9\s]*$/
            },
            address: {
                text: "",
                pattern: /^[a-zA-Z0-9\s]*$/
            },
            phone: {
                text: "",
                pattern: /^[0-9\-]*$/
            }
        };
        $scope.model.contacts = qteFakeContactSvc.getContacts();

        /* Controller methods
         */
        $scope.addContact = function (size) {

            /*
             Intializes Modal used to add a contact
             */
            var addContactModalInstance = $modal.open({
                templateUrl: 'views/_addContact.html',
                controller: 'AddContactCtrl',
                size: size
            });

            /*
             When promise returns form contact information is pushed back on the modal
             */
            addContactModalInstance.result.then(function (contactForm) {
                var contact = qteFakeContactSvc.createContact(contactForm);
                $scope.model.contacts.push(contact);
            }, function () {
                $log.info('Closing Modal: ' + new Date());
            });
        };

        $scope.searchContacts = function (searchTerm) {
            /*
             Searches the array of contacts to match on string searches, not very efficient but will do for a example app
             */
            if (searchTerm) {
                var regEx = new RegExp(searchTerm, "i");
                angular.forEach($scope.model.contacts, function (value, key) {
                    if (regEx.test(value.name) || regEx.test(value.address) || regEx.test(value.phone)) {
                        value.properties.show = true;
                    }
                    else {
                        value.properties.show = false;
                    }
                });
            }
            else {
                angular.forEach($scope.model.contacts, function (value, key) {
                    value.properties.show = true;
                });
            }
            ;
        };
    });
