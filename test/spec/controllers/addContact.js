/**
 * Created by oli on 11.09.14.
 */
'use strict';

describe('Controller: addContactCtrl', function () {

    // load the controller's module
    beforeEach(module('qteApp'));

    var addContactCtrl,
        scope, modalInstance;


    // Initialize the controller and mocks/stubs
    beforeEach(inject(function ($controller, $rootScope) {

        scope = $rootScope.$new();
        modalInstance = jasmine.createSpyObj('modalInstance', ['open', 'close', 'dismiss']);

        addContactCtrl = $controller('AddContactCtrl', {
            $scope: scope,
            $modalInstance: modalInstance
        });
    }));

    it('should have a model defined', function () {
        expect(scope.model).toBeDefined();
        expect(scope.model.newContact).toBeDefined();
    });

    it('should close the modal when finally adding a contact', function () {
        scope.add();
        expect(modalInstance.close).toHaveBeenCalled();
    });
    it('should dismiss the modal when cancel is clicked', function () {
        scope.cancel();
        expect(modalInstance.dismiss).toHaveBeenCalled();
    });
});
