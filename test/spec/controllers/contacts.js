'use strict';

describe('Controller: ContactsCtrl', function () {

    // load the controller's module
    beforeEach(module('qteApp'));

    var ContactsCtrl,
        scope, fakeContactSvc, fakeModal, contacts;

    contacts = [
        {
            name: 'John Smith',
            address: '22 Toronto',
            phone: '528-231-5941',
            properties: {
                show: true
            }
        },
        {
            name: 'Dan McDonald',
            address: '82 Toronto',
            phone: '345-324-9123',
            properties: {
                show: true
            }
        }
    ];

    // Initialize the controller and mocks/stubs
    beforeEach(inject(function ($controller, $rootScope, qteFakeContactSvc, $modal) {

        scope = $rootScope.$new();
        fakeContactSvc = qteFakeContactSvc;
        fakeModal = $modal;

        spyOn(fakeContactSvc, 'getContacts').andReturn(contacts);
        spyOn(fakeModal, 'open').andReturn({
            result: {
                then: function (cb1, cb2) {
                }
            }
        });

        ContactsCtrl = $controller('ContactsCtrl', {
            $scope: scope,
            qteFakeContactSvc: fakeContactSvc,
            $modal : fakeModal
        });
    }));

    it('should have a model defined', function () {
        expect(scope.model).toBeDefined();
    });

    it('should have a search model defined', function () {
        expect(scope.model.search.text).toEqual("");
        expect(scope.model.search.pattern).toBeDefined();
    });


    it('should have a new contact model setup', function () {
        expect(scope.model.newContact.name).toBeDefined();
        expect(scope.model.newContact.address).toBeDefined();
        expect(scope.model.newContact.phone).toBeDefined();
    });

    it('should make a call to qteFakeContactSvc for a list of contacts', function () {
        expect(fakeContactSvc.getContacts).toHaveBeenCalled();
        expect(scope.model.contacts.length).toBe(2);
    });

    it('should be able to search the contacts model and properly set the visibility property', function () {

        expect(scope.searchContacts).toBeDefined();

        scope.searchContacts('John');
        expect(scope.model.contacts[0].properties.show).toEqual(true);
        expect(scope.model.contacts[1].properties.show).toEqual(false);

        scope.searchContacts('Nobody');
        expect(scope.model.contacts[0].properties.show).toEqual(false);
        expect(scope.model.contacts[1].properties.show).toEqual(false);

        scope.searchContacts('Toronto');
        expect(scope.model.contacts[0].properties.show).toEqual(true);
        expect(scope.model.contacts[1].properties.show).toEqual(true);
    });

    it('should be able to add a contact', function () {
        scope.addContact('lg');
        expect(fakeModal.open).toHaveBeenCalled();
    });
});
