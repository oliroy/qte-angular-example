'use strict';

describe('Service: qteFakeContact', function () {

    // load the service's module
    beforeEach(module('qteApp'));

    // instantiate service
    var qteFakeContactSvc, fakeFormModel;

    fakeFormModel = {
        name: {
            text: "Test Name",
            pattern: /^[a-zA-Z0-9\s]*$/
        },
        address: {
            text: "Test Address",
            pattern: /^[a-zA-Z0-9\s]*$/
        },
        phone: {
            text: "111",
            pattern: /^[0-9\-]*$/
        }
    };

    beforeEach(inject(function (_qteFakeContactSvc_) {
        qteFakeContactSvc = _qteFakeContactSvc_;
    }));

    it('should have the proper interface setup', function () {
        expect(qteFakeContactSvc.createContact).toBeDefined();
        expect(qteFakeContactSvc.getContacts).toBeDefined();
    });

    it('should be able to create a contact', function () {

        var contact = qteFakeContactSvc.createContact(fakeFormModel);
        expect(contact.name).toEqual(fakeFormModel.name.text);
        expect(contact.address).toEqual(fakeFormModel.address.text);
        expect(contact.phone).toEqual(fakeFormModel.phone.text);
    });

});
