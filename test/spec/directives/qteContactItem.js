'use strict';

describe('Directive: qteContactItem', function () {

    // load the directive's module
    beforeEach(module('qteApp', 'app/templates/qte-contact-item.html'));

    var element,
        scope, template;

    beforeEach(inject(function ($templateCache, $rootScope, $compile) {

        template = $templateCache.get('app/templates/qte-contact-item.html');
        //include actual directive template
        $templateCache.put('templates/qte-contact-item.html', template);
        //stub out third-party directive template
        $templateCache.put('template/popover/popover.html', "<div></div>");

        scope = $rootScope.$new();
        scope.aContact = {
            name: 'Victoria Wam',
            address: '51 Richmond',
            phone: '905-222-2213',
            properties: {
                show: true
            }
        };

        element = '<qte-contact-item contact="aContact"><qte-contact-item';
        element = $compile(element)(scope);
        scope.$digest();
    }));

    it('should have the proper interface setup', function () {
        var isolatedScope = element.isolateScope();
        expect(isolatedScope.validate).toBeDefined();
        expect(isolatedScope.deleteContact).toBeDefined();
    });

    it('should have the contact model object set', function () {
        var isolatedScope = element.isolateScope();
        expect(isolatedScope.contact).toBeDefined();
        expect(isolatedScope.contact.name).toBe(scope.aContact.name);
        expect(isolatedScope.contact.address).toBe(scope.aContact.address);
    });


    it('should have a delete button setup', function () {
        var button = element.find('button');
        expect(button.text()).toBe('Delete Contact');
    });

});
